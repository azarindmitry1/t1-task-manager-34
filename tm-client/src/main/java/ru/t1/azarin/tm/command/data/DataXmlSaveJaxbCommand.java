package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataXmlSaveJaxbRequest;

public final class DataXmlSaveJaxbCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-save-xml-jaxb";

    @NotNull
    public final static String DESCRIPTION = "Save data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxbRequest request = new DataXmlSaveJaxbRequest(getToken());
        getDomainEndpoint().xmlSaveJaxbResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
