package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataJsonSaveJaxbRequest;

public final class DataJsonSaveJaxbCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-save-json-jaxb";

    @NotNull
    public final static String DESCRIPTION = "Save data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxbRequest request = new DataJsonSaveJaxbRequest(getToken());
        getDomainEndpoint().jsonSaveJaxbResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
