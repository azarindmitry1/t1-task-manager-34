package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.UserRemoveRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-remove";

    @NotNull
    public final static String DESCRIPTION = "Remove user.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().removeResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
