package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataXmlLoadJaxbRequest;

public final class DataXmlLoadJaxbCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-xml-jaxb";

    @NotNull
    public final static String DESCRIPTION = "Load data from xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM XML]");
        @NotNull final DataXmlLoadJaxbRequest request = new DataXmlLoadJaxbRequest(getToken());
        getDomainEndpoint().xmlLoadJaxbResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
