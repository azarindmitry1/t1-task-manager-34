package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.Session;
import ru.t1.azarin.tm.model.User;

public interface IAuthService {

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

}
