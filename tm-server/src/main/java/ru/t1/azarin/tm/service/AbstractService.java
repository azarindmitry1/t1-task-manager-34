package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.IRepository;
import ru.t1.azarin.tm.api.service.IService;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.IndexIncorrectException;
import ru.t1.azarin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @Override
    public @NotNull Collection<M> add(@NotNull Collection<M> models) {
        return repository.add(models);
    }

    @Override
    public @NotNull Collection<M> set(@NotNull Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(id);
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
